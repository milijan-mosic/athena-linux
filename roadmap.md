# Athena Linux v3

- Wayland
- Sway
- i3 Gaps
- Dmenu
- Polybar
- Tmux
- Zsh/Fish
- Lock
- Screensaver
- LightDM

- VS Code
- NeoVim
- Sublime

- Chrome
- Firefox
- RocketChat
- Slack

- Mpv/Vlc
- Mpd
- Nmcpcpp
- Alsa/Pulsewire
- Pavucontrol/?
- Sxiv
- OBS Studio

- Wget
- Curl

- Git
- Lazygit
- Lazydocker
- Npm
- Pip
- Docker
- Docker Compose

- Htop
- NeoMutt
- Ranger
- Nautilus

- FontAwesome
- Iosevka
- FiraCode
- Nerd Fonts

- Gnome Screenshot
- Kamera/Kamoso
- Psensor
- Calculator?
- Transmission
- Ark
- Tar
- ffmpeg
- Openssh
- Gparted
- Gnome Disk Utility
- K3b
- Ufw
- KeepassXC
- Gedit
- Cherrytree/Feathernotes
- Arandr
- LibreOffice
- Zathura
- Acpi
- Redshift

- Man
- Cron
- Ncdu
- Calcurse
- Xwallpaper ?
- VirtualBox
- Termdown
- Ngrok
- OpenVPN
- Timeshift/Borg

- Codecs

