#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias athena_clone='git clone https://gitlab.com/milijan-mosic/athena-linux.git'
alias install_it='athena_clone ; ./athena-linux/install.sh'
alias windwalk="git clone https://gitlab.com/milijan-mosic/athena-linux.git ; sudo bash ./athena-linux/install.sh"

PS1='[\u@\h \W]\$ '
