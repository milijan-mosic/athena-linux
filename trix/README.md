# Trix

## Dependencies

### For using script:

- None! It's running on native Python packages.

### For testing the library

- pytest
- pytest-cov

To test the library, run:

```sh
python -m venv venv
. venv/bin/activate
pip install -U pytest pytest-cov
pip freeze > requirements.txt
clear
pytest --cov
python examples.py
```

## How to write bash script

Start file like this:

```sh
#!/bin/bash


```

**Note**: Shell path + two empty rows

Then proceed with commands:

**Example 1**: _example_script_1.sh_

```sh
#!/bin/bash


ls -ahl
```

**Example 2**: _example_script_2.sh_

```sh
#!/bin/bash


ls $1
```

**Example 3**: _example_script_3.sh_

```sh
#!/bin/bash


ls $1
uname $2
```

## How to run command

```py
from trix import run


# Without args
run('example_script_1.sh')

# With args
run('example_script_2.sh',
    '-ahl')

run('example_script_3.sh',
    '-ahl -a')
```
