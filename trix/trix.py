import subprocess
import logging


# Log levels
CRITICAL: int = 50
ERROR: int = 40
WARNING: int = 30

FORMAT: str = '%(levelname)s -> %(message)s'
logging.basicConfig(format=FORMAT)


def read_commands_from_script(file_path: str) -> list[str] | None:
    if not isinstance(file_path, str) or not file_path:
        logging.log(CRITICAL, f'Argument \'{file_path}\' needs to be type \'str\'!')
        return

    lines: list[str] = []
    try:
        with open(file_path, 'rt') as file:
            lines: list[str] = file.readlines()
    except:
        logging.log(ERROR, f'File with path \'{file_path}\' not found!')
        return

    length_of_lines: int = len(lines)
    if length_of_lines <= 3:
        logging.log(WARNING, f'File with path \'{file_path}\' is an empty script.')
        return

    commands: list[str] = []
    for i in range(3, length_of_lines):
        if not lines[i].startswith('#') and not lines[i].strip('\n') == '':
            commands.append(lines[i].strip('\n'))

    if not commands:
        logging.log(WARNING, f'File with path \'{file_path}\' doesn\'t contain any commands.')
        return

    return commands


def execute_command(command: str | list[str], args: bool = False) -> tuple:
    if args:
        if not isinstance(command, list) or not command:
            logging.log(CRITICAL, f'Argument \'{command}\' needs to be type \'list\'!')
            return (None, None)
    else:
        if not isinstance(command, str):
            logging.log(CRITICAL, f'Argument \'{command}\' needs to be type \'str\'!')
            return (None, None)

    if command == '':
        print('\t\tCommand was the \'\\n\', aborting.')
        return (None, None)

    try:
        if not args:
            command: list[str] = command.split()

        process = subprocess.Popen(command, stdout=subprocess.PIPE, encoding='utf-8')
        return process.communicate()
    except:
        if not args:
            command: str = ' '.join(command)
        logging.log(ERROR, f'Command \"{command}\" cannot be executed.')
        return (None, None)


def print_results(output: str, error: str = None) -> None:
    if not isinstance(output, str) or not output:
        logging.log(CRITICAL, f'Argument \'{output}\' needs to be type \'str\'!')
        return

    print(output[0 : len(output) - 1], '\n')

    if error:
        print('ERROR TYPE -> ', type(error))  # DEBUG
        if not isinstance(error, str):
            logging.log(CRITICAL, f'Argument \'{error}\' needs to be type \'str\'!')
            return

        print(error)

    return


def run(script_path: str, args: str = '') -> None:
    if not isinstance(script_path, str) or not script_path:
        logging.log(CRITICAL, f'Argument \'{script_path}\' needs to be type \'str\'!')
        return

    if args:
        if not isinstance(args, str):
            logging.log(CRITICAL, f'Argument \'{args}\' needs to be type \'str\'!')
            return

        parsed_args: list[str] = args.split(' ')

    print(f'\n\n\tExecuting commands from script \'{script_path}\':\n')
    commands: list[str] = read_commands_from_script(script_path)
    if not commands:
        return

    if args:
        parsed_commands: list[list[str]] = []
        for command in commands:
            parsed_commands.append(command.split(' '))

        arg_counter: int = 0
        for command_list in parsed_commands:
            for command in range(0, len(command_list)):
                if command_list[command].startswith('$1'):
                    print(command_list[command])
                    print(parsed_args[arg_counter])
                    command_list[command] = parsed_args[0]

                elif command_list[command].startswith('$2'):
                    print(command_list[command])
                    print(parsed_args[arg_counter])
                    command_list[command] = parsed_args[1]

                elif command_list[command].startswith('$3'):
                    print(command_list[command])
                    print(parsed_args[arg_counter])
                    command_list[command] = parsed_args[2]

        commands: list[list[str]] = parsed_commands

    executed_any_command: bool = False
    for command in commands:
        output, error = execute_command(command, args=True if args else False)

        if output:
            print_results(output, error)
            executed_any_command = True

    if executed_any_command:
        print('\tDONE!\n')
    else:
        print('\n\tDONE!\n')

    return


# WORK IN PROGRESS!
def jab(command: str) -> None:
    output, error = execute_command(command)

    if output:
        print_results(output, error)
