from trix import run, jab


SCRIPT_DIR_NAME: str = '/root/athena-linux/trix/installations/jabz/'


storage_device: str = '/dev/nvme0n1'
is_uefi: bool = True
device_name: str = 'archy'
root_password: str = '12345'
username: str = 'test'
user_password: str = '12345'
cpu_choice: int = 0
gpu_choice: int = 0


proceed_with_installation: bool = False

while True:
    print('\n')

    try:
        run_defaults: str = str(
            input(
                "\n\n\nDo you want to run this installation with the default values?\nType 'No' or 'Yes':\n\n\n>>> "
            )
        )
    except Exception as e:
        run_defaults: str = ''

    if (
        run_defaults != ''
        and (run_defaults == 'Yes' or run_defaults == 'No')
        and isinstance(run_defaults, str)
    ):
        jab('clear')
        proceed_with_installation = True
        break
    else:
        jab('clear')
        print('\n\nWrong answer or empty input... try again.\n\n')


while not proceed_with_installation:
    # Getting device name on which OS will be installed
    while True:
        print('\n')
        jab('lsblk')

        try:
            storage_device = str(
                input(
                    "\n\n\nWhere do you want to install Athena Linux?\nType in SSD/hard disk device name:\n\n(e.g. '/dev/sda' or '/dev/sdb' etc...)\n\n\n>>> "
                )
            )
        except Exception as e:
            storage_device = ''

        if storage_device != '' and isinstance(storage_device, str) and len(storage_device) >= 8:
            jab('clear')
            break
        else:
            jab('clear')
            print('\n\nInput is not equal to 8 characters or is an empty input... try again.\n\n')

    # Is this UEFI or BIOS mode?
    while True:
        print('\n')
        jab('ls /sys/firmware/efi/efivars')

        try:
            option = int(
                input(
                    "\n\n\nIs this an UEFI or BIOS firmware?\nType '0' for BIOS or '1' for UEFI:\n\n(if 'no directory is found' error is above, it's BIOS)\n\n\n>>> "
                )
            )

            match option:
                case 0:
                    is_uefi = False
                case 1:
                    is_uefi = True
        except Exception as e:
            option = -1

        if (option == 1 or option == 0) and isinstance(option, int):
            jab('clear')
            break
        else:
            jab('clear')
            print('\n\nWrong answer or empty input... try again.\n\n')

    # Getting the device name
    while True:
        try:
            device_name = str(
                input(
                    '\n\nType in desired name for device:\n\n(minimum 3 characters of length)\n\n\n>>> '
                )
            )
        except Exception as e:
            device_name = ''

        if device_name != '' and isinstance(device_name, str) and len(device_name) >= 3:
            jab('clear')
            break
        else:
            jab('clear')
            print('\n\n\n\ndevice_name is too small or empty input... try again.\n\n')

    # Getting the username
    while True:
        try:
            username = str(
                input(
                    '\n\nType in desired username for main user account:\n\n(minimum 3 characters of length)\n\n\n>>> '
                )
            )
        except Exception as e:
            username = ''

        if username != '' and isinstance(username, str) and len(username) >= 3:
            jab('clear')
            break
        else:
            jab('clear')
            print('\n\nUsername is too small or is an empty input... try again.\n\n')

    # Getting the username's password
    while True:
        try:
            user_password = str(
                input(
                    '\n\nType in password for main user account:\n\n(minimum 5 characters of length)\n\n\n>>> '
                )
            )

            confirmed_password: str = str(input('\nType it again >>> '))
        except Exception as e:
            user_password = ''
            confirmed_password: str = ''

        if (
            user_password != ''
            and confirmed_password != ''
            and isinstance(user_password, str)
            and isinstance(confirmed_password, str)
            and len(user_password) >= 5
            and user_password == confirmed_password
        ):
            jab('clear')
            break
        else:
            jab('clear')
            print("\n\nPasswords doesn't match or there are some empty inputs... try again.\n\n")

    # Getting the roots's password
    while True:
        try:
            root_password = str(
                input(
                    '\n\nType in password for root:\n\n(minimum 5 characters of length)\n\n\n>>> '
                )
            )

            confirmed_password: str = str(input('\nType it again >>> '))
        except Exception as e:
            root_password = ''
            confirmed_password: str = ''

        if (
            root_password != ''
            and confirmed_password != ''
            and isinstance(root_password, str)
            and isinstance(confirmed_password, str)
            and len(root_password) >= 5
            and root_password == confirmed_password
        ):
            jab('clear')
            break
        else:
            jab('clear')
            print("\n\nPasswords doesn't match or there are some empty inputs... try again.\n\n")

    # Choosing CPU manufacturer
    while True:
        print('\n')
        jab("grep -m 1 'model name' /proc/cpuinfo")

        try:
            cpu_choice = int(
                input(
                    "\n\nWhich CPU is this device using?\nType '0' for AMD or '1' for Intel:\n\n\n>>> "
                )
            )
        except Exception as e:
            cpu_choice = -1

        if (cpu_choice == 1 or cpu_choice == 0) and isinstance(cpu_choice, int):
            jab('clear')
            break
        else:
            jab('clear')
            print('\n\nWrong answer or empty input... try again.\n\n')

    # Choosing GPU manufacturer
    while True:
        print('\n')
        jab('lspci -v | grep -A1 -e VGA -e 3D')

        try:
            gpu_choice = int(
                input(
                    "\n\n\nWhich GPU is this device using?\nType '0' for AMD, '1' for Intel or '2' for nVidia:\n\n\n>>> "
                )
            )
        except Exception as e:
            gpu_choice = -1

        if (gpu_choice == 0 or gpu_choice == 1 or gpu_choice == 2) and isinstance(gpu_choice, int):
            jab('clear')
            break
        else:
            jab('clear')
            print('\n\nWrong answer or empty input... try again.\n\n')

    # Printing inputs
    while True:
        print(f'\n\nSSD device name: {storage_device}\n')

        if is_uefi:
            print(f'Motherboard firmware: UEFI\n')
        else:
            print(f'Motherboard firmware: BIOS\n')

        print(f'Name for a device: {device_name}\n')

        print(f'Username for main account: {username}\n')

        if cpu_choice == 0:
            print(f'This device uses: AMD CPU\n')
        else:
            print(f'This device uses: Intel CPU\n')

        match gpu_choice:
            case 0:
                print(f'This device uses: AMD GPU\n')
            case 1:
                print(f'This device uses: Intel GPU\n')
            case 2:
                print(f'This device uses: nVidia GPU\n')

        print('Passwords should be kept secret ;)\n\n\n')

        # Final confirmation
        print('Are you sure that this info you entered is correct?')
        print('Do you want to continue with installation?\n')
        print("Type '1' to continue or '0' to enter all information again:\n\n\n>>> ")

        try:
            option: int = int(input())

            match option:
                case 0:
                    proceed_with_installation = False
                case 1:
                    proceed_with_installation = True
        except Exception as e:
            option: int = -1

        if (option == 1 or option == 0) and isinstance(option, int):
            jab('clear')
            break
        else:
            jab('clear')
            print('\n\nWrong answer or empty input... try again.\n\n')


###############
# RUN SCRIPTS #
###############

# run(SCRIPT_DIR_NAME + '1.sh')

# run(SCRIPT_DIR_NAME + '2.sh', storage_device)

# run(SCRIPT_DIR_NAME + '3.sh', storage_device)

# run(SCRIPT_DIR_NAME + '4.sh', f'{cpu_choice} {gpu_choice} {1 if is_uefi else 0}')

# run(SCRIPT_DIR_NAME + '5.sh')

# run(SCRIPT_DIR_NAME + '6.sh')

# run(SCRIPT_DIR_NAME + '7.sh', f'{username} {user_password} {root_password}')

# run(SCRIPT_DIR_NAME + '8.sh', device_name)

# run(SCRIPT_DIR_NAME + '9.sh', f'{1 if is_uefi else 0} {storage_device}')


##############
# MANUAL WAY #
##############


#########
# SETUP #
#########

jab('timedatectl set-ntp true')
jab('reflector --latest 24 --sort rate --save /etc/pacman.d/mirrorlist')


################
# PARTITIONING #
################

# TODO: Ask user for this data...
swap_n: str = '2'
swap_size: str = '+2048M'
# swap_size: str = '+16384M'
swap_type: str = '8200'

root_n: str = '3'
root_size: str = '+10G'
# root_size: str = '+64G'
root_type: str = '8304'

home_n: str = '4'
home_type: str = '8302'

jab(f'sgdisk -Z {storage_device}')
jab(f'sgdisk -p {storage_device}')
jab(f'sgdisk -o {storage_device}')

if is_uefi == True:
    boot_n: str = '1'
    boot_size: str = '+512M'
    boot_type: str = 'EF00'

    jab(f'sgdisk -n {boot_n}:0G:{boot_size} -t {boot_n}:{boot_type} -g {storage_device}')
else:
    bios_boot_n: str = '1'
    bios_boot_size: str = '+1M'
    bios_boot_type: str = 'EF02'

    jab(
        f'sgdisk -n {bios_boot}_n:0:{bios_boot}_size -t {bios_boot}_n:{bios_boot}_type -g {storage_device}'
    )

jab(f'sgdisk -n {swap_n}:0G:{swap_size} -t {swap_n}:{swap_type} -g {storage_device}')
jab(f'sgdisk -n {root_n}:0G:{root_size} -t {root_n}:{root_type} -g {storage_device}')
jab(f'sgdisk -n {home_n}:0G -t {home_n}:{home_type} -g {storage_device}')


##############
# FORMATTING #
##############

if is_uefi == True:
    jab(f"mkfs.fat -F32 {storage_device + 'p1'}")

jab(f"mkswap {storage_device + 'p2'}")
jab(f"swapon {storage_device + 'p2'}")

jab(f"mkfs.ext4 -F {storage_device + 'p3'}")

jab(f"mkfs.ext4 -F {storage_device + 'p4'}")

jab(f"mount {storage_device + 'p3'} /mnt")

if is_uefi == True:
    jab('mkdir /mnt/boot')
    jab('mkdir /mnt/home')

    jab(f"mount {storage_device + 'p1'} /mnt/boot")

else:
    jab(f'mkdir /mnt/home')

jab(f"mount {storage_device + 'p4'} /mnt/home")


################
# INSTALLATION #
################

uefi_package: str = 'efibootmgr'

essential: str = (
    'base base-devel linux linux-firmware util-linux grub archlinux-keyring linux-hardened'
)
desktop_server: str = 'wayland sway swaylock swayidle swaybg python-pywlroots'  # qtile

cog_wheels: str = 'neovim tmux sudo doas alacritty'
internet_drivers: str = 'broadcom-wl networkmanager reflector network-manager-applet wireless_tools wpa_supplicant iw iwd wget git libsecret'

cpu_amd: str = 'amd-ucode'
cpu_intel: str = 'intel-ucode'

gpu_amd: str = 'xf86-video-ati'  # xf86-video-amdgpu
gpu_intel: str = 'xf86-video-intel'
gpu_nvidia: str = 'xf86-video-nouveau'

musthave: str = f'{essential} {desktop_server} {cog_wheels} {internet_drivers}'


drivers: str = ''
match gpu_choice:
    case 0:
        drivers += gpu_amd
    case 1:
        drivers += gpu_intel
    case 2:
        drivers += gpu_nvidia

drivers += ' '
match cpu_choice:
    case 0:
        drivers += cpu_amd
    case 1:
        drivers += cpu_intel

if is_uefi == True:
    packagelist: str = f'{musthave} {drivers} {uefi_package}'
else:
    packagelist: str = f'{musthave} {drivers}'


jab(f'pacstrap /mnt {packagelist}')


##############
# FINALIZING #
##############

jab('genfstab -U /mnt > /mnt/etc/fstab')


##########
# PART 2 #
##########

CHROOT: str = 'arch-chroot'

jab(f'{CHROOT} /mnt timedatectl set-ntp true')
jab(f'{CHROOT} /mnt hwclock -w')

jab(f'{CHROOT} /mnt reflector --latest 100 --sort rate --save /mnt/etc/pacman.d/mirrorlist')


#######################
# SETTING UP TIMEZONE #
#######################

jab(f'{CHROOT} /mnt ln -sf /mnt/usr/share/zoneinfo/Europe/Belgrade /mnt/etc/localtime')
jab(f'{CHROOT} /mnt timedatectl set-timezone Europe/Belgrade')


#############################
# ENABLING SYSTEMD SERVICES #
#############################

jab(f'{CHROOT} /mnt systemctl enable NetworkManager')
# jab('systemctl enable bluetooth.service')


######################################
# CREATING USER & CHANGING PASSWORDS #
######################################

jab(f'{CHROOT} /mnt useradd -m -G wheel -s /mnt/bin/bash {username}')
jab(f'{CHROOT} /mnt gpasswd -a {username} optical')
jab(f'{CHROOT} /mnt gpasswd -a {username} storage')
jab(f'{CHROOT} /mnt gpasswd -a {username} audio')
jab(f'{CHROOT} /mnt gpasswd -a {username} video')
jab(
    f'{CHROOT} /mnt gpasswd -a {username} rfkill'
)  # User could'nt change Bluetooth On/Off without this... weird.
run(SCRIPT_DIR_NAME + 'change_pass_to_users.sh', f'{root_password} {user_password} {username}')


# jab('rm /etc/sudoers')
# jab('cp /Athena-Linux/distro/files/sysfiles/sudoers /etc/')


############################################
# SETTING UP LANGUAGES AND KEYBOARD LAYOUT #
############################################

run(SCRIPT_DIR_NAME + 'set_languages_conf.sh')


############################################
# SETTING UP HOSTNAME AND NETWORK SETTINGS #
############################################

jab(f'{CHROOT} /mnt echo {device_name} > /mnt/etc/hostname')

jab(f'{CHROOT} /mnt rm /mnt/etc/hosts')
jab(
    f'{CHROOT} /mnt wget -P /mnt/etc/ https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts'
)


###########################
# FINALIZING INSTALLATION #
###########################

# jab(f'rm /etc/pacman.conf')
# jab(f'cp /Athena-Linux/distro/files/sysfiles/pacman.conf /etc/')


jab(f'{CHROOT} /mnt pacman -Syu --noconfirm')
jab(f'{CHROOT} /mnt pacman -Scc --noconfirm')


###########################
# SETTING UP A BOOTLOADER #
###########################

if is_uefi == True:
    run(SCRIPT_DIR_NAME + 'install_bootloader_for_uefi.sh')
else:
    run(SCRIPT_DIR_NAME + 'install_bootloader_for_uefi.sh', f'{storage_device}')


jab('echo -e "\nREBOOT HERE!\n"')
# jab('umount -R /mnt')
# jab('reboot')
