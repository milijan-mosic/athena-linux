from trix import run


SCRIPT_DIR_NAME: str = 'scripts/'


run(f'{SCRIPT_DIR_NAME}script_doesnt_exists.sh')

run(f'{SCRIPT_DIR_NAME}empty_script.sh')
run(f'{SCRIPT_DIR_NAME}badly_written_script.sh')
run(f'{SCRIPT_DIR_NAME}commands_commented_out.sh')
run(f'{SCRIPT_DIR_NAME}one_bad_command.sh')
run(f'{SCRIPT_DIR_NAME}multiple_bad_commands.sh')

run(f'{SCRIPT_DIR_NAME}one_hardcoded_command.sh')
run(f'{SCRIPT_DIR_NAME}multiple_hardcoded_commands.sh')
run(f'{SCRIPT_DIR_NAME}one_dynamic_command.sh', '-a')
run(f'{SCRIPT_DIR_NAME}multiple_dynamic_commands.sh', '-a -ahl')

run(f'{SCRIPT_DIR_NAME}all_in_one.sh', 'Aaaaaaaa -ahl -a')
