#!/bin/bash




##############
# FORMATTING #
##############


one="1"
two="2"
three="3"
four="4"

if [ $uefi == 1 ]
then
  temp="$1$one"
  mkfs.fat -F32 $temp
  temp=""
fi

temp="$1$two"
mkswap $temp
swapon $temp
temp=""

temp="$1$three"
mkfs.ext4 -F $temp
temp=""

temp="$1$four"
mkfs.ext4 -F $temp
temp=""

temp="$1$three"
mount $temp /mnt
temp=""

if [ $uefi == 1 ]
then
  mkdir /mnt/boot
  mkdir /mnt/home

  temp="$1$one"
  mount $temp /mnt/boot
  temp=""
else
  mkdir /mnt/home
fi

temp="$1$four"
mount $temp /mnt/home
temp=""
