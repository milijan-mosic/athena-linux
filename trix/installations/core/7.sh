#!/bin/bash




######################################
# CREATING USER & CHANGING PASSWORDS #
######################################


arch-chroot /mnt useradd -m -G wheel -s /mnt/bin/bash $1
arch-chroot /mnt gpasswd -a $1 optical
arch-chroot /mnt gpasswd -a $1 storage
arch-chroot /mnt gpasswd -a $1 audio
arch-chroot /mnt gpasswd -a $1 video
arch-chroot /mnt gpasswd -a $1 rfkill # User could'nt change Bluetooth On/Off without this... weird.
arch-chroot /mnt echo -e "$3\n$3" | arch-chroot /mnt passwd root
arch-chroot /mnt echo -e "$2\n$2" | arch-chroot /mnt passwd $1


# rm /etc/sudoers
# cp /Athena-Linux/distro/files/sysfiles/sudoers /etc/




############################################
# SETTING UP LANGUAGES AND KEYBOARD LAYOUT #
############################################


arch-chroot /mnt sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /mnt/etc/locale.gen
arch-chroot /mnt sed -i 's/#sr_RS UTF-8/sr_RS UTF-8/g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen


arch-chroot /mnt set_lang="LANG=en_US.UTF-8"
arch-chroot /mnt echo $set_lang > /mnt/etc/locale.conf
