#!/bin/bash




################
# PARTITIONING #
################


swap_n="2"
swap_size="+2048M"
# swap_size="+16384M"
swap_type="8200"

root_n="3"
root_size="+10G"
# root_size="+64G"
root_type="8304"

home_n="4"
home_type="8302"

sgdisk -Z $1
sgdisk -p $1
sgdisk -o $1

if [ $uefi == 1 ]
then
  boot_n="1"
  boot_size="+512M"
  boot_type="EF00"

  sgdisk -n $boot_n:0G:$boot_size -t $boot_n:$boot_type -g $1
else
  bios_boot_n="1"
  bios_boot_size="+1M"
  bios_boot_type="EF02"

  sgdisk -n $bios_boot_n:0:$bios_boot_size -t $bios_boot_n:$bios_boot_type -g $1
fi

sgdisk -n $swap_n:0G:$swap_size -t $swap_n:$swap_type -g $1
sgdisk -n $root_n:0G:$root_size -t $root_n:$root_type -g $1
sgdisk -n $home_n:0G -t $home_n:$home_type -g $1
