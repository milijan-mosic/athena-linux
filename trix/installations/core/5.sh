#!/bin/bash




##############
# FINALIZING #
##############


genfstab -U /mnt > /mnt/etc/fstab




##########
# PART 2 #
##########


arch-chroot /mnt timedatectl set-ntp true
arch-chroot /mnt hwclock -w


arch-chroot /mnt reflector --latest 100 --sort rate --save /mnt/etc/pacman.d/mirrorlist
