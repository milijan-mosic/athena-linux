#!/bin/bash




##############
# USER INPUT #
##############


clear

got_user_input=false
while [ $got_user_input == false ]
do
  pass=false
  while [ $pass == false ]
  do
    echo -e "\n" ; lsblk
    echo -e "\n\n\nWhere do you want to install Athena Linux?\nType in SSD/hard disk device name:\n\n(e.g. '/dev/sda' or '/dev/sdb' etc...)\n\n\n>>>"
    read ssd

    length=${#ssd}
    if [ $length -ge 8 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nInput is not equal to 8 characters or is an empty input... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    echo -e "\n" ; ls /sys/firmware/efi/efivars
    echo -e "\n\n\nIs this an UEFI or BIOS firmware?\nType '0' for BIOS or '1' for UEFI:\n\n(if 'no directory is found' error is above, it's BIOS)\n\n\n>>>"
    read uefi

    if [ $uefi == 1 ] || [ $uefi == 0 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nWrong answer or empty input... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    echo -e "\n\nType in desired name for device:\n\n(minimum 3 characters of length)\n\n\n>>>"
    read hostname

    length=${#hostname}
    if [ $length -ge 3 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nHostname is too small or empty input... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    echo -e "\n\nType in desired username for main user account:\n\n(minimum 3 characters of length)\n\n\n>>>"
    read username

    length=${#username}
    if [ $length -ge 3 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nUsername is too small or is an empty input... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    user_password=""
    echo -e "\n\nType in password for main user account:\n\n(minimum 5 characters of length)\n\n\n>>>"
    read -s user_password1
    echo -e "\nType it again >>>"
    read -s user_password2

    if [ $user_password1 == $user_password2 ]; then
      pass_length1=${#user_password1}
      pass_length2=${#user_password2}
      if [ $pass_length1 -ge 5 ] && [ $pass_length2 -ge 5 ]; then
  pass=true
  user_password=$user_password1
  clear
      else
  echo -e "\n\nPassword is too small.\n\n"
      fi
    else
      clear
      echo -e "\n\nPasswords doesn't match or there are some empty inputs... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    root_password=""
    echo -e "\n\nType in password for root:\n\n(minimum 5 characters of length)\n\n\n>>>"
    read -s root_password1
    echo -e "\nType it again >>>"
    read -s root_password2

    if [ $root_password1 == $user_password2 ]; then
      pass_length1=${#root_password1}
      pass_length2=${#root_password2}
      if [ $pass_length1 -ge 5 ] && [ $pass_length2 -ge 5 ]; then
  pass=true
  root_password=$root_password1
  clear
      else
  echo -e "\n\nPassword is too small.\n\n"
      fi
    else
      clear
      echo -e "\n\nPasswords doesn't match or there are some empty inputs... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    # Is there a way to show which CPU computer/laptop has?
    echo -e "\n\nWhich CPU is this device using?\nType '0' for AMD or '1' for Intel:\n\n\n>>>"
    read cpu_choice

    if [ $cpu_choice == 1 ] || [ $cpu_choice == 0 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nWrong answer or empty input... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    echo -e "\n" ; lspci -v | grep -A1 -e VGA -e 3D
    echo -e "\n\n\nWhich GPU is this device using?\nType '0' for AMD, '1' for Intel or '2' for nVidia:\n\n\n>>>"
    read gpu_choice

    if [ $gpu_choice == 0 ] || [ $gpu_choice == 1 ] || [ $gpu_choice == 2 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nWrong answer or empty input... try again.\n\n"
    fi
  done


  pass=false
  while [ $pass == false ]
  do
    echo -e "\n\nSSD device name: $ssd\n"

    if [ $uefi == 1 ]; then
      echo -e "Motherboard firmware: UEFI\n"
    else
      echo -e "Motherboard firmware: BIOS\n"
    fi

    echo -e "Name for a device: $hostname\n"
    echo -e "Username for main account: $username\n"

    if [ $cpu_choice == 0 ]; then
      echo -e "This device uses: AMD CPU\n"
    else
      echo -e "This device uses: Intel CPU\n"
    fi

    if [ $gpu_choice == 0 ]; then
      echo -e "This device uses: AMD GPU\n"
    else
      if [ $gpu_choice == 1 ]; then
  echo -e "This device uses: Intel GPU\n"
      else
  echo -e "This device uses: nVidia GPU\n"
      fi
    fi

    echo -e "Passwords should be kept secret ;)\n\n\n"




    echo -e "Are you sure that this info you entered is correct?"
    echo -e "Do you want to continue with installation?\n"
    echo -e "Type '1' to continue or '0' to enter all information again:\n\n\n>>>"
    read got_user_input

    if [ $got_user_input == 1 ] || [ $got_user_input == 0 ]; then
      pass=true
      clear
    else
      clear
      echo -e "\n\nWrong answer or empty input... try again.\n\n"
    fi
  done
done


ssd="/dev/sda"
uefi=1
hostname="voidbox"
root_password=1234
username="windwalk"
user_password=1234
cpu_choice=0
gpu_choice=0
