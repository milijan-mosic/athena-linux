#!/bin/bash




#######################
# SETTING UP TIMEZONE #
#######################


arch-chroot /mnt ln -sf /mnt/usr/share/zoneinfo/Europe/Belgrade /mnt/etc/localtime
arch-chroot /mnt timedatectl set-timezone Europe/Belgrade




#############################
# ENABLING SYSTEMD SERVICES #
#############################


arch-chroot /mnt systemctl enable NetworkManager
# systemctl enable bluetooth.service
