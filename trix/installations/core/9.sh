#!/bin/bash




###########################
# SETTING UP A BOOTLOADER #
###########################


if [ $1 == 1 ]
then
  arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
  arch-chroot /mnt grub-mkconfig -o /mnt/boot/grub/grub.cfg
else
  arch-chroot /mnt grub-install --target=i386-pc $2
  arch-chroot /mnt grub-mkconfig -o /mnt/boot/grub/grub.cfg
fi

arch-chroot /mnt sed -i 's/set timeout=5/set timeout=0.1/g' /mnt/boot/grub/grub.cfg




echo -e "\nREBOOT HERE!\n"
# arch-chroot /mnt
umount -R /mnt
# reboot
echo -e "\nREBOOT!\n"
