#!/bin/bash




################
# INSTALLATION #
################


uefi_package="efibootmgr"

essential="base base-devel linux linux-firmware util-linux grub archlinux-keyring linux-hardened"
desktop_server="wayland sway swaylock swayidle swaybg python-pywlroots" # qtile

cog_wheels="neovim tmux sudo doas alacritty"
internet_drivers="broadcom-wl networkmanager reflector network-manager-applet wireless_tools wpa_supplicant iw iwd wget git libsecret"

cpu_amd="amd-ucode"
cpu_intel="intel-ucode"

gpu_amd="xf86-video-ati" # xf86-video-amdgpu
gpu_intel="xf86-video-intel"
gpu_nvidia="xf86-video-nouveau"


musthave="$essential $desktop_server $cog_wheels $internet_drivers"




case "$2" in
  0) drivers="$gpu_amd";;
  1) drivers="$gpu_intel";;
  2) drivers="$gpu_nvidia";;
esac

space=" "
drivers="$space"

case "$1" in
  0) drivers="$cpu_amd";;
  1) drivers="$cpu_intel";;
esac




if [ $3 == 1 ]
then
  packagelist="$musthave $drivers $uefi_package"
else
  packagelist="$musthave $drivers"
fi


pacstrap /mnt $packagelist
