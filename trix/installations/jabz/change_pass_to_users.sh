#!/bin/bash




######################################
# CREATING USER & CHANGING PASSWORDS #
######################################

arch-chroot /mnt echo -e "$1\n$1" | arch-chroot /mnt passwd root
arch-chroot /mnt echo -e "$2\n$2" | arch-chroot /mnt passwd $3
