#!/bin/bash




###########################
# SETTING UP A BOOTLOADER #
###########################

arch-chroot /mnt grub-install --target=i386-pc $1
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

arch-chroot /mnt sed -i 's/set timeout=5/set timeout=0.1/g' /boot/grub/grub.cfg
