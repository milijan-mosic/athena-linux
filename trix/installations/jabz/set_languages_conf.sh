#!/bin/bash




############################################
# SETTING UP LANGUAGES AND KEYBOARD LAYOUT #
############################################

arch-chroot /mnt sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
arch-chroot /mnt sed -i 's/#sr_RS UTF-8/sr_RS UTF-8/g' /etc/locale.gen
arch-chroot /mnt locale-gen


arch-chroot /mnt echo LANG=en_US.UTF-8 > /etc/locale.conf
