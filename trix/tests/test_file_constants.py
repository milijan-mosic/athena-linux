from ..trix import CRITICAL, ERROR, WARNING, FORMAT


def test_are_constants_set_up():
    assert isinstance(CRITICAL, int) and CRITICAL == 50
    assert isinstance(ERROR, int) and ERROR == 40
    assert isinstance(WARNING, int) and WARNING == 30
    assert isinstance(FORMAT, str)
