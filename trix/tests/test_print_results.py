from ..trix import print_results


def test_passing_args():
    assert print_results(1) == None
    assert print_results(1.11) == None
    assert print_results(True) == None
    assert print_results((1, 1)) == None
    assert print_results('') == None
    assert print_results({'msg': 'hey'}) == None
    assert print_results({}) == None
    assert print_results([]) == None
    assert print_results(None) == None
    assert print_results(b'a') == None

    assert print_results(1, 1) == None
    assert print_results(1.11, 1.11) == None
    assert print_results(True, True) == None
    assert print_results((1, 1), (1, 1)) == None
    assert print_results('asd', '') == None
    assert print_results({'msg': 'hey'}, {'msg': 'hey'}) == None
    assert print_results({}, {}) == None
    assert print_results([], []) == None
    assert print_results(None, None) == None
    assert print_results(b'a', b'a') == None


def test_printing_results():
    assert print_results('wasd') == None
    assert print_results('wasd', 'qwerty') == None
