from ..trix import read_commands_from_script


SCRIPT_DIR_NAME: str = 'scripts/'


def test_passing_args():
    assert read_commands_from_script(1) == None
    assert read_commands_from_script(1.11) == None
    assert read_commands_from_script(True) == None
    assert read_commands_from_script((1, 1)) == None
    assert read_commands_from_script('') == None
    assert read_commands_from_script({'msg': 'hey'}) == None
    assert read_commands_from_script({}) == None
    assert read_commands_from_script([]) == None
    assert read_commands_from_script(None) == None
    assert read_commands_from_script(b'a') == None


def test_reading_files():
    assert read_commands_from_script(
        f'{SCRIPT_DIR_NAME}non_existing_script.sh') == None

    assert read_commands_from_script(
        f'{SCRIPT_DIR_NAME}empty_script.sh') == None

    assert read_commands_from_script(
        f'{SCRIPT_DIR_NAME}badly_written_script.sh') == None

    assert read_commands_from_script(
        f'{SCRIPT_DIR_NAME}commands_commented_out.sh') == None

    res = read_commands_from_script(
        f'{SCRIPT_DIR_NAME}one_bad_command.sh')
    assert res == ['cd xxx'] and \
        isinstance(res, list) and \
        isinstance(res[0], str)

    res = read_commands_from_script(
        f'{SCRIPT_DIR_NAME}one_hardcoded_command.sh')
    assert res == ['uname'] and \
        isinstance(res, list) and \
        isinstance(res[0], str)
