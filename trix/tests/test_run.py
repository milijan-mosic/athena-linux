from ..trix import run


SCRIPT_DIR_NAME: str = 'scripts/'


def test_passing_args():
    assert run(1) == None
    assert run(1.11) == None
    assert run(True) == None
    assert run((1, 1)) == None
    assert run('') == None
    assert run({'msg': 'hey'}) == None
    assert run({}) == None
    assert run([]) == None
    assert run(None) == None
    assert run(b'a') == None

    assert run(1, 1) == None
    assert run(1.11, 1.11) == None
    assert run(True, True) == None
    assert run((1, 1), (1, 1)) == None
    assert run('asd', '') == None
    assert run({'msg': 'hey'}, {'msg': 'hey'}) == None
    assert run({}, {}) == None
    assert run([], []) == None
    assert run(None, None) == None
    assert run(b'a', b'a') == None


def test_running_commands_from_files():
    assert run(f'{SCRIPT_DIR_NAME}script_doesnt_exists.sh') == None

    assert run(f'{SCRIPT_DIR_NAME}empty_script.sh') == None
    assert run(f'{SCRIPT_DIR_NAME}badly_written_script.sh') == None
    assert run(f'{SCRIPT_DIR_NAME}commands_commented_out.sh') == None
    assert run(f'{SCRIPT_DIR_NAME}one_bad_command.sh') == None
    assert run(f'{SCRIPT_DIR_NAME}multiple_bad_commands.sh') == None

    assert run(f'{SCRIPT_DIR_NAME}one_hardcoded_command.sh') == None
    assert run(f'{SCRIPT_DIR_NAME}multiple_hardcoded_commands.sh') == None

    assert run(f'{SCRIPT_DIR_NAME}one_dynamic_command.sh',
               '-a') == None

    assert run(f'{SCRIPT_DIR_NAME}multiple_dynamic_commands.sh',
               '-a -ahl') == None

    assert run(f'{SCRIPT_DIR_NAME}all_in_one.sh',
               'Aaaaaaaa -ahl -a') == None
