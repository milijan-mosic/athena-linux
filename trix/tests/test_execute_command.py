from ..trix import execute_command


def test_passing_args():
    assert execute_command(1) == (None, None)
    assert execute_command(1.11) == (None, None)
    assert execute_command(True) == (None, None)
    assert execute_command((1, 1)) == (None, None)
    assert execute_command('') == (None, None)
    assert execute_command({'msg': 'hey'}) == (None, None)
    assert execute_command({}) == (None, None)
    assert execute_command([]) == (None, None)
    assert execute_command(None) == (None, None)
    assert execute_command(b'a') == (None, None)

    assert execute_command(1, True) == (None, None)
    assert execute_command(1.11, True) == (None, None)
    assert execute_command(True, True) == (None, None)
    assert execute_command((1, 1), True) == (None, None)
    assert execute_command('asd', True) == (None, None)
    assert execute_command({'msg': 'hey'}, True) == (None, None)
    assert execute_command([], True) == (None, None)  # lines 60-62
    assert execute_command(None, True) == (None, None)
    assert execute_command(b'a', True) == (None, None)


def test_executing_command():
    assert execute_command('uname') == ('Linux\n', None)
    assert execute_command('cd xxx') == (None, None)

    assert execute_command(['echo', 'Hello'], True) == ('Hello\n', None)
    assert execute_command(['cd', 'xxx'], True) == (None, None)
